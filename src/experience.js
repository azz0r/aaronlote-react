import React from "react"

const Experience = () => (
  <ul id="experience">
    <li tabIndex={0}>
      <strong>10 years of commercial web development</strong> experience
    </li>
    <li tabIndex={0}>
      Delivered BT Mobile using <strong>React JS</strong>
    </li>
    <li tabIndex={0}>
      <strong>ES6+</strong> &gt; ES5/ES3 transpilation
    </li>
    <li tabIndex={0}>
      Enzyme component <strong>unit testing</strong> paired with Mocha & Chai
    </li>
    <li tabIndex={0}>
      5 years experience managing web development teams (<strong>
        PDP, 1:1, L&D, HR
      </strong>)
    </li>

    <li tabIndex={0}>
      Task runners such as Gulp & Grunt & module bundler{" "}
      <strong>Webpack</strong>
    </li>
    <li tabIndex={0}>
      <strong>SASS</strong> & LESS compiling, Bootstrap integration
    </li>
    <li tabIndex={0}>
      Qualified <strong>Agile and Scrum Certification</strong> and Training
      completed with Mike Cohn
    </li>
    <li tabIndex={0}>
      Delivered an <strong>internal CMS & SPA experiences</strong> (Backstage,
      Stage) to empower theAudience team and its clients
    </li>
    <li tabIndex={0}>
      Advanced understanding of SEO and techniques to improve search ranking
    </li>
    <li tabIndex={0}>
      Past experience in developing PHP APIs using{" "}
      <strong>mongoDB and MySQL</strong> with workBench
    </li>
    <li tabIndex={0}>
      Advising and implementing process to{" "}
      <strong>improve website speed</strong> (ySlow, Google PageSpeed,
      minification, bundling, image optimisations)
    </li>
    <li tabIndex={0}>
      Excellent mobile experience; ensuring BT.com is{" "}
      <strong>responsive</strong> across mobile, tablet and desktop
    </li>
    <li tabIndex={0}>
      DevOps experience advising on deployment processes, server set up &
      requirements
    </li>
    <li tabIndex={0}>
      Running a democratic <strong>design council</strong> with senior front-end
      developers to vote on code rules & processes, discuss tools & libraries
      and agree on frameworks to use
    </li>
    <li tabIndex={0}>
      Created and enforced code standards by putting{" "}
      <strong>pull request & reviews</strong> at the core of code delivery.
      Seconded only by enforcing code lint standards pass 100% before being able
      to commit
    </li>
    <li tabIndex={0}>
      Technically advised on <strong>Backbone, React</strong> & Angular project
      deliveries
    </li>
    <li tabIndex={0}>
      Create an internal task force to help BT host JavaScript meet ups inside
      BT (JS Meet up currently hosted)
    </li>
    <li tabIndex={0}>
      Providing high level estimates for business critical projects
    </li>
    <li tabIndex={0}>
      Running ceremonies such as{" "}
      <strong>stand-up, retrospectives, sizing</strong>
    </li>
  </ul>
)

export default Experience
